﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Security.Cryptography;

namespace Server
{
    class Server
    {
        const int keySize = 1024;
        static string publicAndPrivateKey;
        static string publicKey;
        static TcpListener server = null;
        static TcpClient client = new TcpClient();
        static void establishServer()
        {
            try
            {

                int port = 8888;
                server = new TcpListener(IPAddress.Any, port);
                server.Start();
                while (!client.Connected)
                {
                    Console.WriteLine("Menunggu koneksi...");
                    client = server.AcceptTcpClient();
                    Console.WriteLine("Koneksi Established!");
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException", e);
                Console.Read();
            }
        }
        static void StartConnection()
        {
            NetworkStream stream = client.GetStream();
            byte[] _publicKey = Encoding.ASCII.GetBytes(publicKey);
            stream.Write(_publicKey, 0, _publicKey.Length);
            Console.WriteLine("Mengirim Public Key");
            Console.WriteLine(publicKey);
            byte[] _encrypted = new byte[1024];
            stream.Read(_encrypted, 0, _encrypted.Length);
            _encrypted = _encrypted.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
            string encrypted = Encoding.ASCII.GetString(_encrypted);
            string decrypted = AsymmetricEncryption.EncryptText(encrypted, keySize, publicAndPrivateKey);
            Console.WriteLine("Decrypted: {0}", decrypted);

            //Receive Message
            /* byte[] _publicKey = Encoding.ASCII.GetBytes(publicKey);
             stream.Write(_publicKey, 0, _publicKey.Length);
             byte[] _encryptedMsg = new byte[1024];
             stream.Read(_encryptedMsg, 0,_encryptedMsg.Length);
             _encryptedMsg = _encryptedMsg.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
             string encrypted = Encoding.ASCII.GetString(_encryptedMsg);
             Console.WriteLine("Encrypted Message: {0}", encrypted);
             string decrypted = AsymmetricEncryption.DecryptText(encrypted, keySize, publicAndPrivateKey);
             Console.WriteLine("Decrypted: {0}", decrypted);
             Console.WriteLine("============================"); */
            //Send Message
            /* _publicKey = new byte[1024];
             stream.Read(_publicKey, 0, _publicKey.Length);
             _publicKey = _publicKey.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
             Console.WriteLine("Masukkan text untuk di enkripsi: ");
             string publicKeyReceiver;
             publicKeyReceiver = Encoding.ASCII.GetString(_publicKey);
             string text = Console.ReadLine();
             encrypted = AsymmetricEncryption.EncryptText(text, keySize, publicKeyReceiver);
             Console.WriteLine("Encrypted: {0}", encrypted);
             byte[] _encrypted = Encoding.ASCII.GetBytes(encrypted);
             stream.Write(_encrypted, 0, _encrypted.Length); */
        }

         static void Main(string[] args)
         {
            string plain = "Something you want to keep private with Triple DES";

            string encrypted = CipherUtility.Encrypt<TripleDESCryptoServiceProvider>(plain, "password", "salt");

            string decrypted = CipherUtility.Decrypt<TripleDESCryptoServiceProvider>(encrypted, "password", "salt");
            Console.WriteLine(decrypted);
            AsymmetricEncryption.Certificate(keySize, out publicKey, out publicAndPrivateKey);
             establishServer();
             try
             {
                StartConnection();

             }
             catch (SocketException e)
             {
                 Console.WriteLine("SocketException", e);
                 Console.Read();
             }
             client.Close();
         }
     }
 }
