﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Client
{
    class Client
    {
        static TcpClient myClient = new TcpClient();
        static void establishConnection(int port, string ip = "127.0.0.1")
        {
            try
            {
                myClient.Connect(IPAddress.Parse(ip), port);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error...", e.StackTrace.ToString());
            }
        }
        static void Main()
        {
            const int keySize = 1024;
            string publicAndPrivateKey;
            string publicKey;
            AsymmetricEncryption.Certificate(keySize, out publicKey, out publicAndPrivateKey);
            
            Console.WriteLine("Masukkan IP Tujuan: ");
            string ipTarget = Console.ReadLine();
            establishConnection(8888, ipTarget);
            try
            {
                NetworkStream stream = myClient.GetStream();
                byte[] _publicKey = new byte[1024];
                Console.WriteLine("Menerima Public Key");
                stream.Read(_publicKey, 0, _publicKey.Length);
                string publicKeyReceiver;
                publicKeyReceiver = Encoding.ASCII.GetString(_publicKey);
                Console.WriteLine(publicKeyReceiver);
                Console.WriteLine("Public Key :", publicKeyReceiver);
                Console.WriteLine("Masukkan Secret Key: ");
                string text = Console.ReadLine();
                string encrypted = AsymmetricEncryption.EncryptText(text, keySize, publicKeyReceiver);
                Console.WriteLine("Encrypted: {0}", encrypted);
                byte[] _encrypted = Encoding.ASCII.GetBytes(encrypted);
                stream.Write(_encrypted, 0, _encrypted.Length);
                
                while (true)
                {
                    
                    //Send
                    /*  byte[] _publicKey = new byte[1024];
                      stream.Read(_publicKey, 0, _publicKey.Length);
                      _publicKey = _publicKey.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
                      Console.WriteLine("Masukkan text untuk di enkripsi: ");
                      string publicKeyReceiver;
                      publicKeyReceiver = Encoding.ASCII.GetString(_publicKey);
                      string text = Console.ReadLine();
                      string encrypted = AsymmetricEncryption.EncryptText(text, keySize, publicKeyReceiver);
                      Console.WriteLine("Encrypted: {0}", encrypted);
                      byte[] _encrypted = Encoding.ASCII.GetBytes(encrypted);
                      stream.Write(_encrypted, 0, _encrypted.Length);
                      Console.WriteLine("============================");*/
                    //Receive
                   
                    /*byte[] _encryptedMsg = new byte[1024];
                    stream.Read(_encryptedMsg, 0, _encryptedMsg.Length);
                    _encryptedMsg = _encryptedMsg.Reverse().SkipWhile(x => x == 0).Reverse().ToArray();
                    string encrypted = Encoding.ASCII.GetString(_encryptedMsg);
                    Console.WriteLine("Encrypted Message: {0}", _publicKey);
                    string decrypted = AsymmetricEncryption.DecryptText(encrypted, keySize, publicAndPrivateKey);
                    Console.WriteLine("Decrypted: {0}", decrypted); */
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error...", e.StackTrace.ToString());
            }
            myClient.Close();
        }
    }
}
